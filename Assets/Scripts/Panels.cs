using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panels : MonoBehaviour
{
    [SerializeField] GameObject[] panels;
    int panelIndex;

    void Start()
    {
        panelIndex = 0;

        //Los pone todos en falso y activa solo el primero [index 0]
        for (int i = 0; i < panels.Length; i++) panels[i].SetActive(false);
        panels[panelIndex].SetActive(true);
    }

    public void NextPanel()
    {
        //Lo desactiva, sube el index y activa el siguiente
        panels[panelIndex].SetActive(false);
        panelIndex++;
        panels[panelIndex].SetActive(true);
    }

    public void PreviousPanel()
    {
        //Lo desactiva, baja el index y activa el anterior
        panels[panelIndex].SetActive(false);
        panelIndex--;
        panels[panelIndex].SetActive(true);
    }
}
