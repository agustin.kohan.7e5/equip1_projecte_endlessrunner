using UnityEngine;
using UnityEngine.UI;

public class HabilityUseState : MonoBehaviour
{
    private Image _sr;
    public bool State;

    private void Start()
    {
        _sr = GetComponent<Image>();
    }

    public void Activate()
    {
        //18 00 FF Hex = 24 00 255 Dec
        _sr.color = new Color(24, 00, 255);
    }

    public void DeActivate()
    {
        _sr.color = Color.black;
    }
}
