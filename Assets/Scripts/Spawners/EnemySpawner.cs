using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    GameObject[] _enemyes;

    // Start is called before the first frame update
    void Awake()
    {
        SetRandomPosition();
        SpawnNewEnemy();
    }

    private void SetRandomPosition()
    {
        int randonNum = Random.Range(-7, 7);
        transform.position = new Vector2(transform.parent.position.x + randonNum, transform.position.y);
        //Debug.Log(randonNum);
    }

    private void SpawnNewEnemy()
    {
        GameObject _enemy = ChoseRandomEnemy();
        Instantiate(_enemy, transform);
    }
    private GameObject ChoseRandomEnemy()
    {
        return _enemyes[Random.Range(0, _enemyes.Length - 1)];
    }
}
