using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawnerState
{
    Active,
    Waiting,
    Disabled,
}

public class BlockSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject[] _blocks;

    [SerializeField]
    GameObject[] _blocksTutorial;

    [SerializeField]
    GameObject[] _blocksBoss;

    private float _spawnTimeLapse;
    public float spawnTimeRatio;
    private float _timer;

    SpawnerState _spawnerState;
    private int _tutorialBlock;

    GameObject _block;

    
    // Start is called before the first frame update
    void Start()
    {
        _tutorialBlock = -1;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_spawnerState)
        {
            case SpawnerState.Active:
                SpawnNewBlock();
                break;
            case SpawnerState.Waiting:
                _timer += Time.deltaTime;
                if (_timer >= _spawnTimeLapse)
                    _spawnerState = SpawnerState.Active;                
                break;
            case SpawnerState.Disabled:
                break;
            default:
                break;
        }
        _spawnTimeLapse = spawnTimeRatio / GameManager.Instance.GameplaySpeed;
    }

    private void SpawnNewBlock()
    {
        _timer = 0;
        GameObject _block = ChoseRandomBlock();
        Instantiate(_block, transform);
        //Debug.Log("Intancio la " + _block.name);
        _spawnerState = SpawnerState.Waiting;
    }

    private GameObject ChoseRandomBlock()
    {
        switch (GameManager.Instance.GameFase)
        {
            case GameFase.Tutorial:
                // Els block del Tutorial es posaran en ordre, la resta sera random.
                _tutorialBlock++;
                if (_tutorialBlock < _blocksTutorial.Length) _block = _blocksTutorial[_tutorialBlock];
                break;
            case GameFase.Level:
                do
                {
                    _block = _blocks[Random.Range(0, _blocks.Length - 1)];
                } while (
                    // Condicions:
                    // CurrentColorFilter = 1 (pinchos invisibles)
                    GameManager.Instance.CurrentColorFilter == 1 && _block.transform.Find("BloqueDePinchos") != null ||
                    GameManager.Instance.CurrentColorFilter != 1 && _block.transform.Find("InvisibleBloqueDePinchos") != null ||
                    // CurrentColorFilter = 2 (yoyos invisibles)
                    GameManager.Instance.CurrentColorFilter == 2 && _block.transform.Find("Yoyo") != null ||
                    GameManager.Instance.CurrentColorFilter != 2 && _block.transform.Find("InvisibleYoyo") != null ||
                    // CurrentColorFilter = 3 (puentes invisibles)
                    GameManager.Instance.CurrentColorFilter == 3 && _block.transform.Find("PuenteDeBloques") != null ||
                    GameManager.Instance.CurrentColorFilter != 3 && _block.transform.Find("InvisiblePuenteDeBloques") != null
                    // IMPORTANTE Si falla revisar nombre en prefab, algunos ten�an numeros al final. 
                );
                break;
            case GameFase.Boss:
                _block = _blocksBoss[Random.Range(0, _blocksBoss.Length - 1)];
                break;
            default:
                _block = _blocks[Random.Range(0, _blocks.Length-1)];
                break;
        }
        return _block;
    }
}
