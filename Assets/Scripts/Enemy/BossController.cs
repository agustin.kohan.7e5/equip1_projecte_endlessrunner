using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BossPosition
{
    Up,
    Center,
    Down
}

public enum BossAttack //Llista amb els atacs del boss
{
    Nothing,
    // Fase 1
    Shoot, 
    // Fase 2
    ChangeColor,
    // Fase 3
    DropEnemy
}

public enum BossFase // Fases que te el boss
{
    First,    // Vida +70%
    Second,   // Vida +40%
    Third,    // Vida +0
    Dead      // Vida 0
}
public class BossController : Character
{
    //private bool _upDown; // Descomentar per a BossPosition.Center
    private BossPosition _bossPosition;
    private Vector3 _minPosition; // Camera.main.ScreenToWorldPoint(new Vector3(600, 100, 1))
    //private Vector3 _centerPosition; // Camera.main.ScreenToWorldPoint(new Vector3(600, 200, 1))
    private Vector3 _maxPosition; // Camera.main.ScreenToWorldPoint(new Vector3(600, 300, 1))
    private float _time;
    private float _timeAnimationMovement = 3f; // Temps del moviment, velocitat
    public Object Bullet;

    public float _timeAttack = 3f;
    public float _timeNextAttack;
    public BossFase _bossFase;

    public int _attack;

    //public GameObject Enemy;
    public Transform _lastBlock;
    public GameObject BlockSpawner;

    //
    private int childCount=0;
    //

    [SerializeField]
    private GameObject[] Enemys;

    [SerializeField] AudioClip apareceClip, chasqueaClip, muereClip;
    [SerializeField] AudioSource source;

    //private float TotalBossLife = 1000f;
    //public float BossLife;

    // Start is called before the first frame update
    protected override void Awake()
    {
        //En principio esto sirve para pillar el borde de la camara
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        Vector3 bordePantalla = cam.ViewportToWorldPoint(new Vector3(1, 0.5f, cam.nearClipPlane));

        //Para que aparezca un poquito más detras y entre poco a poco
        transform.position = bordePantalla + new Vector3(5,0,0);
        //transform.position = Camera.main.ScreenToWorldPoint(new Vector3(1000, 700, 1)); // Posici� fora de la pantalla.
        _bossPosition = BossPosition.Down;
        //_upDown = false;
        _timeNextAttack = 6f;
        _bossFase = BossFase.First;
        BlockSpawner = GameObject.Find("BlockSpawner");
        source.PlayOneShot(apareceClip);
        base.Awake();
    }

    // Update is called once per frame
    void Update()
    {
        // Hola soy Iv�n un gusto profe que corrija esto
        AnimationMovement();
        ChangeBossFase();

        _timeNextAttack -= Time.deltaTime;
        if (_timeNextAttack <= 0)
        {
            _timeNextAttack = _timeAttack;
            Attack();
        }
    }

    private void AnimationMovement()
    {
        // Posiciones seg�n la pantalla
        //_minPosition = Camera.main.ScreenToWorldPoint(new Vector3(800, 100, 1)); 
        //_centerPosition = Camera.main.ScreenToWorldPoint(new Vector3(600, 200, 1));
        //_maxPosition = Camera.main.ScreenToWorldPoint(new Vector3(800, 430, 1)); 

        // Como el mundo no se mueve (sino las plataformas) se pueden usar posiciones fijas.
        _minPosition = new Vector3(8f,2f,1f);
        _maxPosition = new Vector3(8f,8f,1f);

        _time = Time.deltaTime * _timeAnimationMovement; 


        switch (_bossPosition)
        {
            case BossPosition.Up:
                transform.position = Vector3.MoveTowards(transform.position, _maxPosition, _time);
                if (transform.position == _maxPosition)
                {
                    _bossPosition = BossPosition.Down;
                }
                break;
            case BossPosition.Down:
                transform.position = Vector3.MoveTowards(transform.position, _minPosition, _time);
                if (transform.position == _minPosition)
                {
                    _bossPosition = BossPosition.Up;
                }
                break;
        }
    }

    private void Attack()
    {
        switch (_bossFase)
        {
            case BossFase.First:
                _attack = Random.Range(1, (int)BossAttack.Shoot + 1); // Random entre 1 i l'ultim element d'atacs fase 1. S'HA DE CANVIAR MANUALMENT.
                break;
            case BossFase.Second:
                _attack = Random.Range(1, (int)BossAttack.ChangeColor + 1); // Random entre 1 i l'ultim element d'atacs fase 2. S'HA DE CANVIAR MANUALMENT.
                break;
            case BossFase.Third:
                _attack = Random.Range(1, System.Enum.GetValues(typeof(BossAttack)).Length); // Random entre 1 y l'ultim element del enum BossAttack.
                break;
            case BossFase.Dead:
                Die();
                // Canviar nova distancia d'aparicio del boss
                GameManager.Instance.NewDistanceBossApparenance = GameManager.Instance.DistanceTravelled + GameManager.Instance.DistanceBossApparenance;
                GameManager.Instance.IsBossCreated = false;
                break;
        }

        switch ((BossAttack)_attack)
        {
            case BossAttack.Shoot:
                _anim.SetTrigger("Shoot");
                //Shoot();
                break;
            case BossAttack.ChangeColor:
                _anim.SetTrigger("SwitchColors");
                //ChangeColor();
                break;
            case BossAttack.DropEnemy:
                _anim.SetTrigger("SpawnEnemy");
                //DropEnemy();
                break;
        }
    }

    private void ChangeBossFase()
    {
        if (_currentHealth <= 0)
        {
            _bossFase = BossFase.Dead;
            GameManager.Instance.GameFase = GameFase.Level;
        }
        else if (_currentHealth > _maxHealth * 0.7f) _bossFase = BossFase.First;
        else if (_currentHealth > _maxHealth * 0.4f) _bossFase = BossFase.Second;
        else _bossFase = BossFase.Third;
    }

    private void Shoot()
    {
        Instantiate(Bullet, new Vector3(transform.position.x -  5f, transform.position.y, transform.position.z), new Quaternion(0f, 0f, 0f, 0f));

    }

    private void DropEnemy()
    {
        int indexEnemy = Random.Range(0, Enemys.Length - 1);
        childCount = BlockSpawner.transform.childCount;
        _lastBlock = BlockSpawner.transform.GetChild(BlockSpawner.transform.childCount - 1);
        Enemys[indexEnemy].GetComponent<BossAttackManager>().Attack = BossAttack.DropEnemy;
        Enemys[indexEnemy].GetComponent<BossAttackManager>().LastBlock = _lastBlock;
        Instantiate(Enemys[indexEnemy], new Vector3(transform.position.x - 2, transform.position.y, transform.position.z), new Quaternion(0f, 0f, 0f, 0f));
    }

    private void ChangeColor()
    {
        float auxVolume = source.volume;
        source.volume = source.maxDistance;
        source.PlayOneShot(chasqueaClip);
        source.volume = auxVolume;
        int randomNum = 0;
        do
        {
            randomNum = Random.Range(1, 4);
            Debug.Log(randomNum);
        } while (GameManager.Instance.CurrentColorFilter == randomNum);
        GameManager.Instance.CurrentColorFilter = randomNum;
    }

    public override void Die()
    {
        StartCoroutine(MorirGrito());
        base.Die();
    }

    IEnumerator MorirGrito()
    {
        source.PlayOneShot(muereClip);
        //yield return new WaitForSeconds(muereClip.length);
        yield return new WaitForSeconds(3);
    }
}