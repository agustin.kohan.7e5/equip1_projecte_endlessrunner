using UnityEngine;



public class BossAttackManager : MonoBehaviour
{
    
    // Controlar els dispars del boss, perque el dispar del player no funciona en ell.

    public BossAttack Attack;
    [SerializeField] float bossDamage;
    private Rigidbody2D _rigidbody;
    private Vector3 _bossPosition;

    private float _time;
    private float _timeLife = 10f;

    // Bullets
    public float SpeedProjectiles;

    // Enemys
    public Transform LastBlock;

    // Start is called before the first frame update
    void Awake()
    {
        /*if (GameObject.Find("Boss(Clone)"))
        {
            _bossPosition = GameObject.Find("Boss(Clone)").transform.position;
            _time = 0f;
        }*/
        if((BossAttack) Attack != BossAttack.Nothing) _bossPosition = GameObject.Find("Boss(Clone)").transform.position;
        _time = 0f;

        switch (Attack)
        {
            case BossAttack.Shoot:
                Shoot();
                break;
            case BossAttack.DropEnemy:
                DropEnemy();
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;
        if (_time >= _timeLife)
        {
            Destroy(this.gameObject);
        }
    }

    private void Shoot()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();
        _rigidbody.AddForce(new Vector2(_bossPosition.x * SpeedProjectiles, _bossPosition.y * SpeedProjectiles));
    }

    private void DropEnemy()
    {
        transform.SetParent(LastBlock);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageable target = collision.GetComponent<IDamageable>();
        if (target != null && collision.gameObject.CompareTag("Player"))
        {
            target.TakeDamage(bossDamage, "s");
        }
        //if (collision.CompareTag("Player")) collision.gameObject.GetComponent<Character>().ReciveDamage(15);
    }
}
