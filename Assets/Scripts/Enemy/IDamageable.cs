using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    //El damageType es porque dependiendo de como se ataque hace una cosa u otra
    public void TakeDamage(float damageTaken, string damageType);
}
