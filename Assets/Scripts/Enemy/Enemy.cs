using UnityEngine;

public class Enemy : Character
{
    [SerializeField] float _contactDamage;
    [SerializeField] float _hitScore;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Da�o por contacto DE los enemigos AL jugador
        IDamageable target = collision.GetComponent<IDamageable>();

        //if (target != null && (!collision.CompareTag("Enemy") && !collision.CompareTag("Boss"))) target.TakeDamage(_contactDamage,"m");
        if (target != null && collision.CompareTag("Player")) target.TakeDamage(_contactDamage,"m");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si no es una peonza se hace kinematic (no le afectan las fisicas)
        if (!this.gameObject.GetComponent<PeonzaBehaviour>())
        {
            if (collision.gameObject.tag == "Ground")
            {
                this._rb.isKinematic = true;
                this._collider.isTrigger = true;
            }
        }
    }

    //Sobreescribe, usa la base + le suma puntuaci�n
    public override void TakeDamage(float damageTaken, string damageType)
    {
        base.TakeDamage(damageTaken, damageType);

        //Dependiendo de si le ataca con Shoot o Melee da diferentes puntuaciones
        if (damageType == "s") GameManager.Instance.Score += (_hitScore / 2);
        if (damageType == "m") GameManager.Instance.Score += _hitScore;
    }
}
