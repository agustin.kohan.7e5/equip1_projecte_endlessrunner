using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FadeOut : MonoBehaviour
{
    private SpriteRenderer _sr;
    [SerializeField] private float _totalTimeBeeingVisible;
    [SerializeField] private float _fadeOutSpeed;
    [SerializeField] private float _timeBeeingVisible;

    private Color _originalColor;

    [SerializeField] private bool _fadeOut;
    [SerializeField] List<GameObject> lista = new List<GameObject>();

    void Start()
    {
        //Si tiene uno lo coje y hace lo suyo
        if (GetComponent<SpriteRenderer>())
        {
            _sr = GetComponent<SpriteRenderer>();
        }
        else
        {
            //Revisa todos los hijos y se guarda en una lista los que tengan sprite renderer
            //buscarSR(transform, lista);
            _sr = GetComponentInChildren<SpriteRenderer>();
        }
        _fadeOut = false;
        if (_sr != null) _originalColor = _sr.color;
    }
    
    /*
    private void buscarSR(Transform buscarEn, List<GameObject> guardarEn)
    {
        foreach (Transform childTransform in transform)
        {
            if (childTransform.childCount > 0) buscarSR(transform, guardarEn);
            if (childTransform.GetComponent<SpriteRenderer>()) lista.Add(childTransform.gameObject);
        }
    } */

    private void Update()
    {
        if (_fadeOut) Fade();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Sonar"))
        {
            _timeBeeingVisible = _totalTimeBeeingVisible;
            _fadeOut = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Sonar"))
        {
            _fadeOut = false;
            _sr.color = _originalColor;
        }
    }

    private void Fade()
    {
        if (_timeBeeingVisible <= 0)
        {
            if (_sr.color.a >= 0) 
            {
                _sr.color -= new Color(0, 0, 0, _fadeOutSpeed) * Time.deltaTime; 
            }
        }
        else _timeBeeingVisible -= Time.deltaTime;
    }
}
