using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour, IDamageable
{
    [SerializeField]
    public float _maxHealth;
    [SerializeField]
    public float _currentHealth;
    protected BoxCollider2D _collider;
    protected Rigidbody2D _rb;
    protected Animator _anim;


    protected virtual void Awake()
    {
        _currentHealth = _maxHealth;
        _collider = GetComponent<BoxCollider2D>();
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
    }

    /*
    private IEnumerator DestroyOnAnimationEnd()
    {
        AnimationClip[] clips = _anim.runtimeAnimatorController.animationClips;
        _anim.SetTrigger("Die");

        foreach (AnimationClip clip in clips)
            if (clip.name == "DieAnimation") yield return new WaitForSeconds(clip.length);

        Destroy(gameObject);
    }
    */

    public virtual void Die()
    {
        if (this is Player)
        {
            SceneManager.LoadScene("GameOverScene_Test");
            DontDestroyOnLoad(GameObject.Find("ScoreHolder"));
        }
        else
        {
            Destroy(this.gameObject);
            //StartCoroutine(DestroyOnAnimationEnd());
        } 
    }

    public virtual void TakeDamage(float damageTaken, string damageType)
    {
        _anim.SetTrigger("Damaged");
        _currentHealth -= damageTaken;

        if (_currentHealth <= 0)        {            //Dependiendo de la manera de matarlos los enemigos dan una recompensa u otra            switch (damageType)            {                case "s":                    //Cosas que hace si muere por disparo (sumar puntuaci�n YA lo hace)                    break;                case "m":                    //Cosas que hace si muere por ataque a melee (sumar puntuaci�n YA lo hace)                    GameManager.Instance.AddUses(1);                    break;            }            _anim.SetTrigger("Die");            //Die();        }
    }
}
