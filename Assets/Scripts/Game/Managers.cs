using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Managers : MonoBehaviour
{
    private static Managers instance = null;
    //Game Instance Singleton

    public static Managers Instance { get { return instance; } }
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;
    }
}
