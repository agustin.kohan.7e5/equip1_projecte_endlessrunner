using UnityEngine;
using UnityEngine.SceneManagement;

public class UIButtonController : MonoBehaviour
{
    public void ChangeScene(string Scene) => SceneManager.LoadScene(Scene);
    public void ExitGame() => Application.Quit();
}
