using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    private static UIManager instance = null;

    //Game Instance Singleton

    public static UIManager Instance { get { return instance; } }

    void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;

        //Tiene que estar en c�digo para el replay, si se pone arrastrando al recargar la escena no se mantiene
        _scoreText = GameObject.Find("ScoreNum").GetComponent<TextMeshProUGUI>();
        _vidaText = GameObject.Find("VidaTxt").GetComponent<Text>();
        _vidaSlider = GameObject.Find("VidaSlider").GetComponent<Slider>();
        _player = GameObject.Find("Player").GetComponent<Player>();
    }

    GameObject _canvas;
    public TextMeshProUGUI _scoreText;
    
    public Text _vidaText;
    public Slider _vidaSlider;

    public Player _player;

    // Start is called before the first frame update
    void Start()
    {
        _canvas = GameObject.Find("Canvas");
        _scoreText = GameObject.Find("ScoreText").GetComponent<TextMeshProUGUI>();
        _vidaSlider.maxValue = _player._maxHealth;

        //Tiene que estar en c�digo para el replay, si se pone arrastrando al recargar la escena no se mantiene
        _scoreText = GameObject.Find("ScoreNum").GetComponent<TextMeshProUGUI>();
        _vidaText = GameObject.Find("VidaTxt").GetComponent<Text>();
        _vidaSlider = GameObject.Find("VidaSlider").GetComponent<Slider>();
        _player = GameObject.Find("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        _vidaText.text = _player._currentHealth.ToString();
        _vidaSlider.value = _player._currentHealth;
    }

    // v = m/s ==> m = v * s
    public void UpdateScore(float score)
    {
        _scoreText.text = ((int)score).ToString();
    }
}
