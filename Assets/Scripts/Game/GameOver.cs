using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Text scoreTxt;
    [SerializeField] Text newRTxt;

    private void Start()
    {
        //Por defecto 'New Record' estar� desactivado
        newRTxt.gameObject.SetActive(false);

        //Se muestra la puntuaci�n
        scoreTxt.text = Mathf.Round(GameObject.Find("ScoreHolder").GetComponent<ScoreHolder>().score).ToString();

        //Si la puntuaci�n actual supera la m�xima, se sobreescribe en el game manager y se muestra 'New Record'
        if (GameManager.Instance.Score > PlayerPrefs.GetFloat("topScore"))
        {
            PlayerPrefs.SetFloat("topScore",GameManager.Instance.Score);
            newRTxt.gameObject.SetActive(true);
        }
    }
}
