using UnityEngine;

public class DestroyWhenOutOfLimits : MonoBehaviour
{
    [SerializeField]
    Vector2 _bounds = new Vector2(1,1);

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < Camera.main.ScreenToWorldPoint(Vector2.zero).x - _bounds.x || transform.position.y < Camera.main.ScreenToWorldPoint(Vector2.zero).y - _bounds.y)
        {
            if (gameObject.GetComponent<Character>()) this.gameObject.GetComponent<Character>()?.Die();
            else Destroy(this.gameObject);
        }
    }
}
