using System.Collections.Generic;
using UnityEngine;

public enum GameFase
{
    Tutorial,
    Level,
    Boss
}

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    //Game Instance Singleton

    public static GameManager Instance { get { return instance; } }
    void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;

        Score = 0;
        GameFase = GameFase.Tutorial;
        NewDistanceBossApparenance = DistanceBossApparenance;
    }

    public float Score;
    public float MaxScore;

    public float GameplaySpeed;
    public float DistanceTravelled;
    [SerializeField]
    private Wilberforce.Colorblind colorblind;
    public int CurrentColorFilter;
    
    //
    public int HabilityUses;
    [SerializeField] private List<GameObject> _usesList;
    [SerializeField] private SonarHability _sonar;
    //

    public GameObject Boss;
    public bool IsBossCreated;
    public float DistanceBossApparenance;
    public float NewDistanceBossApparenance;

    public GameFase GameFase;

    // Start is called before the first frame update
    void Start()
    {
        DistanceTravelled = 0;
        IsBossCreated = false;
        colorblind.Type = CurrentColorFilter = 1;
        FillUses();
        GameFase = GameFase.Tutorial;
        NewDistanceBossApparenance = DistanceBossApparenance;
    }

    // Update is called once per frame
    void Update()
    {
        // v = m/s ==> m = v * s
        DistanceTravelled += GameplaySpeed * Time.deltaTime;
        Score += GameplaySpeed * Time.deltaTime;

        UIManager.Instance.UpdateScore(Score);
        colorblind.Type = CurrentColorFilter;

        Hability();

        UpdateGameFase();
        RestartGameFase();
    }

    public void AddUses(int addedUses)
    {
        //As� funciona, pero se tendria que mirar de dejarlo bien organizado
        //Para evitar estos problemas tendria que mirar centralizar lo que pasa al pulsar H (tanto que tire la habilidad
        //como modificar la interfaz (cargas))

        if (HabilityUses < 0) HabilityUses = addedUses;  
        else
        { 
            //Si al sumar usos de habilidades se pasa del limite, se establece como el limite
            if (HabilityUses + addedUses < _usesList.Count + 1) HabilityUses += addedUses;
            else HabilityUses = _usesList.Count; 
        }

        FillUses();
    }

    public void FillUses()
    {
        //Primero las pone todas en falso
        for (int i = 0; i < _usesList.Count; i++) _usesList[i].GetComponent<HabilityUseState>().DeActivate();
        
        //Y luego llena las que hagan falta
        for (int i = 0; i < HabilityUses; i++) _usesList[i].GetComponent<HabilityUseState>().Activate();
    }

    void Hability()
    {
        if (HabilityUses == 0) HabilityUses = -15; //�apa para arreglar el uso limitado de la habilidad

        //Mirar de hacerlo con eventos
        if (Input.GetButtonDown("Sonar") && HabilityUses > 0)
        { 
            if (_sonar.hability == SonarHability.HabilityStates.nothing)
            {
                HabilityUses--;
                FillUses();   
            } 
        }
    }

    void UpdateGameFase()
    {
        if (GameFase != GameFase.Tutorial)
        {
            if (DistanceTravelled >= NewDistanceBossApparenance && IsBossCreated == false) GameFase = GameFase.Boss;
            else if (IsBossCreated == false) GameFase = GameFase.Level;
        }
        else
        {
            // Detectar que la plataformas de tutorial han acabado.
            // Crea un GameObject al final de la última plataforma del Tutorial llamado "Flag" y Triger, al cruzarla se cambiara la fase de juego.
            if (GameObject.Find("Flag") != null)
                if (GameObject.Find("Player").GetComponent<BoxCollider2D>().IsTouching(GameObject.Find("Flag").GetComponent<BoxCollider2D>()))
                    GameFase = GameFase.Level;
        }
       
    }

    void RestartGameFase()
    {
        switch (GameFase)
        {
            case GameFase.Tutorial:
                break;
            case GameFase.Level:
                break;
            case GameFase.Boss:
                AppearBoss();
                break;
        }
    }

    void AppearBoss()
    {
        if (!IsBossCreated)
        {
            Instantiate(Boss, new Vector3(9f,0f,0f), transform.rotation);
            IsBossCreated = true;
        }
    }
}
