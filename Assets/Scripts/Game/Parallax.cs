using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float _length, _startPos;
    public GameObject _cam;
    private float _dist;
    private float _parallaxEffect;
    private float _temp;

    // Start is called before the first frame update
    void Start()
    {
        
        _length = GetComponent<SpriteRenderer>().bounds.size.x * 2;
        _startPos = _length;
        _parallaxEffect = 0.05f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _temp = transform.position.x + _length;
        _dist = GameManager.Instance.DistanceTravelled;
        transform.position = new Vector3(transform.position.x - _parallaxEffect, transform.position.y, transform.position.z);

        if (transform.position.x < _startPos - _length*2) transform.position = new Vector3(_length, transform.position.y, transform.position.z);
    }
}
