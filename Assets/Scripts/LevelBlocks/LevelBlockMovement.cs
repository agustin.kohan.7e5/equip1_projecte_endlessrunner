using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBlockMovement : MonoBehaviour
{
    private void FixedUpdate()
    {
        transform.position -= new Vector3(GameManager.Instance.GameplaySpeed * Time.deltaTime, 0f, 0f);
    }
}
