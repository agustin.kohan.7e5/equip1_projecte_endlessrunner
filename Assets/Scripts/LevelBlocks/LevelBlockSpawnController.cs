using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBlockSpawnController : MonoBehaviour
{
    [SerializeField] private GameObject[] _levelBlocks;
    private int _randomBlock;

    [SerializeField] private float _timeBetweenBlock;
    private float _cooldown;

    void Start()
    {
        _cooldown = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (_cooldown <= 0)
        {
            _randomBlock = Random.Range(0, _levelBlocks.Length + 1);
            Debug.Log(_randomBlock);


            Instantiate(_levelBlocks[_randomBlock], new Vector3(transform.position.x, transform.position.y,transform.position.z), Quaternion.identity);
            _cooldown = _timeBetweenBlock;
        }
        else _cooldown -= Time.deltaTime;
    }
}
