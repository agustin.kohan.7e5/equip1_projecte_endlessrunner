using UnityEngine;

public class PeonzaBehaviour : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private Collider2D colliderSolido;
    [SerializeField] private Rigidbody2D rb;

    void Start()
    {
        _speed = -_speed; //Negativo para que vaya a la izq
    }

    void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.position += new Vector3(_speed * Time.deltaTime,0,0);
    }

    /*Lo que hacen los OnCollision, si choca con el jugador para que no lo arrastre la parte "solida" (para que no se caiga del mapa)
    se hace trigger por un momento, y se hace kinematic para que no se caiga, al salir vuelve */

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            colliderSolido.isTrigger = true;
            rb.isKinematic = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            colliderSolido.isTrigger = false;
            rb.isKinematic = false;
        }
    }
}
