using UnityEngine;

public class ObstacleDamage : MonoBehaviour
{
    [SerializeField] float _contactDamage;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.GetComponent<PeonzaBehaviour>())
        {
            IDamageable target = collision.GetComponent<IDamageable>();
            if (target != null) target.TakeDamage(_contactDamage,"m");
        }
    }
}
