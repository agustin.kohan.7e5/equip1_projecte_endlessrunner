using UnityEngine;

public abstract class Item : MonoBehaviour
{
    [SerializeField] AudioClip itemClip;
    [SerializeField] AudioSource source;

    private void Awake()
    {
        source = GameObject.Find("Player").GetComponent<AudioSource>();
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            source.PlayOneShot(itemClip);
            ApplyItemEffect(collision);
            Destroy(this.gameObject);
        }
    }
    protected abstract void ApplyItemEffect(Collider2D collision); 
}
