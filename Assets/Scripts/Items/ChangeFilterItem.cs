using UnityEngine;

public class ChangeFilterItem : Item
{
    private int PreviousColorFilter;

    private void Start()
    {
        PreviousColorFilter = GameManager.Instance.CurrentColorFilter;
    }
    
    protected override void ApplyItemEffect(Collider2D collision)
    {
        int randomNum = 0;
        do
        {
            randomNum = Random.Range(1, 4);
            Debug.Log(randomNum);

        } while (GameManager.Instance.CurrentColorFilter == randomNum);

        GameManager.Instance.CurrentColorFilter = randomNum;
    }
}
