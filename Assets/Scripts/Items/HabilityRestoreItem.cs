using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabilityRestoreItem : Item
{
    protected override void ApplyItemEffect(Collider2D collision)
    {
       GameManager.Instance.AddUses(1);
    }
}
