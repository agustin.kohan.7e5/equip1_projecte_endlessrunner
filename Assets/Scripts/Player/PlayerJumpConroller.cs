using UnityEngine;

public class PlayerJumpConroller : MonoBehaviour
{
    public float JumpForce;

    Animator _anim;
    BoxCollider2D _collider2d;
    Rigidbody2D _rb;
    RaycastHit2D hit1;
    RaycastHit2D hit2;
    // Start is called before the first frame update

    public AudioClip jumpClip;
    public AudioSource AudioSource;

    void Start()
    {
        _anim = GetComponent<Animator>();
        _collider2d = GetComponent<BoxCollider2D>();
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
            Jump();
        _anim.SetFloat("VerticalSpeed", _rb.velocity.y);
    }

    void Jump()
    {
        hit1 = Physics2D.Raycast(new Vector2(_collider2d.bounds.min.x, _collider2d.bounds.min.y), Vector2.down, 0.15f);
        hit2 = Physics2D.Raycast(new Vector2(_collider2d.bounds.max.x, _collider2d.bounds.min.y), Vector2.down, 0.15f);

        Debug.DrawRay(new Vector2(_collider2d.bounds.min.x, _collider2d.bounds.min.y), Vector2.down, Color.red, 0.15f);
        Debug.DrawRay(new Vector2(_collider2d.bounds.max.x, _collider2d.bounds.min.y), Vector2.down, Color.red, 0.15f);

        if (hit1.collider?.tag == "Ground" || hit2.collider?.tag == "Ground")
        {
            _rb.AddForce(new Vector2(0, JumpForce * 100));
            _anim.SetTrigger("Jump");

            AudioSource.PlayOneShot(jumpClip);
        }
    }
}
