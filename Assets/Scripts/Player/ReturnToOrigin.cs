using UnityEngine;

public class ReturnToOrigin : MonoBehaviour
{
    float _originXPosition;

    private void Awake()
    {
        _originXPosition = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < _originXPosition)
            transform.position += new Vector3(0.01f, 0);
    }
}
