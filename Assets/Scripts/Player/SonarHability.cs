using UnityEngine;

public class SonarHability : MonoBehaviour
{
    [SerializeField] private float _habilitySpeed; //La velocidad del sonar
    [SerializeField] private float _habilityRange; //El rango que abarca el sonar
    [SerializeField] private float _habilityCooldown;
    private float _cooldown;
    private Vector3 _maxHabilitySize;
    private Vector3 _originalSize;

    [SerializeField] public HabilityStates hability; //La "maquina de estados" que maneja la funcionalidad del sonar

    [SerializeField] AudioClip sonarClip;
    [SerializeField] AudioSource source;

    public enum HabilityStates
    {
        nothing,
        runing,
        backToOriginal
    }

    private void Start()
    {
        hability = HabilityStates.nothing;
        _maxHabilitySize = transform.localScale * _habilityRange;
        _originalSize = transform.localScale;
    }

    private void LateUpdate()
    {
        //Como es LateUpdate primero se resta el uses, asi que cuando seria el uso 1 es 0
        if (Input.GetButtonDown("Sonar") && hability == HabilityStates.nothing && GameManager.Instance.HabilityUses > -1)
        {
            source.PlayOneShot(sonarClip);
            hability = HabilityStates.runing;
        }

        switch (hability)
        {
            case HabilityStates.runing:
                if (transform.localScale.x < _maxHabilitySize.x) transform.localScale += new Vector3(_habilitySpeed, _habilitySpeed, 0) * Time.deltaTime;
                else
                {
                    hability = HabilityStates.backToOriginal;
                    _cooldown = _habilityCooldown;
                }
                break;
            case HabilityStates.backToOriginal:
                transform.localScale = _originalSize;
                if (_cooldown <= 0) hability = HabilityStates.nothing;
                else _cooldown -= Time.deltaTime;
                break;
        }

    }
}
