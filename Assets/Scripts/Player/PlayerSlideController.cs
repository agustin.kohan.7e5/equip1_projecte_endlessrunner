using System;
using System.Collections;
using UnityEngine;

public class PlayerSlideController : MonoBehaviour
{
    [SerializeField]
    float _slideDuration;

    private Vector2 _standColliderSize;
    private Vector2 _standColliderOffset;
    private Vector2 _slideColliderSize;
    private Vector2 _slideColliderOffset;
    private BoxCollider2D _collider2d;
    private float _offset;
    
    private bool _canSlide;
    Animator _anim;

    [Range(0,1)]
    public float CrouchPorcentage;
    public bool IsCrouching { get; private set; }

    public AudioClip slideClip;
    public AudioSource AudioSource;


    // Start is called before the first frame update
    void Start()
    {
        _anim = GetComponent<Animator>();
        _canSlide = true;
    }

    private void Awake()
    {
        _collider2d = GetComponent<BoxCollider2D>();
        _standColliderSize = _collider2d.size;
        _standColliderOffset = _collider2d.offset;

        _slideColliderSize = new Vector2(_standColliderSize.x, _standColliderSize.y * CrouchPorcentage);
        _offset = _standColliderSize.y - _slideColliderSize.y;
        _slideColliderOffset = new Vector2(_standColliderOffset.x, (_standColliderOffset.y - _offset) / 2);
    }

    // Update is called once per frame
    void Update()
    {
        Slide();
    }

    private void Slide()
    {
        if (Input.GetKeyDown(KeyCode.S) && _canSlide)
        {
            
            IsCrouching = true;
            _canSlide = false;
            _collider2d.size = _slideColliderSize;
            _collider2d.offset = _slideColliderOffset;
            StartCoroutine(Sliding());

            AudioSource.PlayOneShot(slideClip);
        }
    }

    private IEnumerator Sliding()
    {
        _anim.SetBool("Slide", true);
        yield return new WaitForSeconds(_slideDuration);
        _anim.SetBool("Slide", false);
        IsCrouching = false;
        _canSlide = true;
        _collider2d.size = _standColliderSize;
        _collider2d.offset = _standColliderOffset;
    }
}
