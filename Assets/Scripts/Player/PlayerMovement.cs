using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    Rigidbody2D _rb;
    public float Speed;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        MoveForward();
    }

    private void MoveForward()
    {
        _rb.velocity = new Vector2(Speed * Time.deltaTime * 10, _rb.velocity.y);
    }
}
