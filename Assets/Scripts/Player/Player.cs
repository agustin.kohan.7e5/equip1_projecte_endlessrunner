using System.Collections;
using UnityEngine;

public class Player : Character
{
    [SerializeField]
    private float _intangibleTime;
    private bool _isIntangible = false;

    public AudioClip hitClip;
    public AudioSource AudioSource;

    public override void TakeDamage(float damageTaken, string typeDamage)
    {
        if (!_isIntangible) base.TakeDamage(damageTaken, typeDamage);
        StartCoroutine(BecomeIntangible());

        AudioSource.PlayOneShot(hitClip);
    }

    IEnumerator BecomeIntangible()
    {
        _isIntangible = true;
        yield return new WaitForSeconds(_intangibleTime);
        _isIntangible = false;
    }
}
