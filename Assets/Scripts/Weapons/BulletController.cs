using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float BulletSpeed;
    
    Rigidbody2D _rb;
    [SerializeField]
    private float _timeLife;
    private float _timer = 0;

    public float BulletDamage;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        float z = transform.eulerAngles.z;
        _rb.velocity = BulletSpeed * new Vector2(Mathf.Cos(z * Mathf.Deg2Rad), Mathf.Sin(z * Mathf.Deg2Rad));
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        if (_timer >= _timeLife) Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Da�o A los enemigos
        IDamageable target = collision.GetComponent<IDamageable>();

        if (target != null)
        {
            target.TakeDamage(BulletDamage,"s");
            Destroy(this.gameObject);
        }   
    }
}
