using UnityEngine;

public class KnifeController : MonoBehaviour
{
    public float Damage;
    [SerializeField] PlayerAttackController player;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageable target = collision.GetComponent<IDamageable>();
        if (target != null) target.TakeDamage(Damage,"m");
    }
}
