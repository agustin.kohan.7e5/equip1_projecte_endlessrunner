using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackController : MonoBehaviour
{
    enum AttackStsate
    {
        Active,
        Disabled
    }

    [SerializeField]
    AttackStsate _attackState = AttackStsate.Active;
    BoxCollider2D _collider2d;
    RaycastHit2D hit;
    [SerializeField]
    float _attackCooldown;

    [SerializeField]
    protected GameObject _bulletPrefab;
    private GameObject _bullet;
    [SerializeField]
    protected float _distanceDamage;

    KnifeController _knifeController;
    [SerializeField]
    public float _meleeDamage;

    private Animator _anim;

    public AudioClip shootClip, meleeAttackClip;
    public AudioSource AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        _collider2d = GetComponent<BoxCollider2D>();
        _knifeController = transform.Find("Knife").GetComponent<KnifeController>();
        _knifeController.Damage = _meleeDamage;
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && _attackState == AttackStsate.Active) Attack();
    }

    private void Attack()
    {
        hit = Physics2D.Raycast(new Vector2(_collider2d.bounds.max.x, _collider2d.bounds.center.y), Vector2.right, 1f);
        Debug.DrawRay(new Vector2(_collider2d.bounds.max.x, _collider2d.bounds.center.y), Vector2.right, Color.red, 1f);

        if (hit.collider?.tag == "Enemy") MeleeAttack();
        else Shoot();
    }

    private void MeleeAttack()
    {
        _anim.SetTrigger("Melee");
        _knifeController.gameObject.SetActive(true);
        StartCoroutine(Attacking());

        AudioSource.PlayOneShot(meleeAttackClip);
    }

    public virtual void Shoot()
    {
        _anim.SetTrigger("Shoot");
         _bullet = Instantiate(_bulletPrefab, new Vector2(_collider2d.bounds.max.x + 0.5f, _collider2d.bounds.center.y), transform.rotation);
        _bullet.GetComponent<BulletController>().BulletDamage = _distanceDamage;
        StartCoroutine(Attacking());

        AudioSource.PlayOneShot(shootClip);
    }

    IEnumerator Attacking()
    {
        _attackState = AttackStsate.Disabled;
        yield return new WaitForSeconds(_attackCooldown);
        _attackState = AttackStsate.Active;
        _knifeController.gameObject.SetActive(false);
    }

}
